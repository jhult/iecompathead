﻿# IECompatHead

## Component Information
* Author: Ken Walker
* Updated by: Jonathan Hult
* Last Updated: build_1_20140109

## Overview
IE is a troublesome browser, in that functionality changes from version to version, and sometimes you must set a compatibility variable back to a specific version so that web pages will function properly. This is usually set up, from an end user perspective, via the Compatibility View section of the options page in IE. However, this needs to be done on a per-user basis, and can be problematic for sites with corporate intranets where forcing these administrative actions on users is not a viable option.

This component needs to extend the standard page head section to include html that:

* Respects non-IE browsers – don’t add anything if the browser is not IE: <https://stackoverflow.com/questions/19784822/does-meta-http-equiv-x-ua-compatible-content-ie-edge-impact-on-non-ie-explor>
* If the browser is IE, set the compatibility for EmulateIE8.

<https://blogs.oracle.com/proactivesupportWCC/entry/how_to_create_a_wcc>

* Dynamichtml includes:
	- std_html_head_declarations: Override to include EmulateIE8 if on IE 9+
	
* Preference Prompts:
	- IECompatHead_ComponentEnabled: Component enabled?
	
## Compatibility
This component was built upon and tested on the version listed below, this is the only version it is known to work on, but it may work on older/newer versions.

* 11.1.1.8.0PSU-2013-08-27 18:40:56Z-r109125 (Build: 7.3.5.185)

## Changelog
* build_1_20140109
	- Initial component release